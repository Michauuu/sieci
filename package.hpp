// Paweł Kolendo, 302860
// Michał Kasperek, 302857
// Michał Gorczyca, 302846

#ifndef SIECI_PACKAGE_HPP
#define SIECI_PACKAGE_HPP

#include "types.hpp"

#include <iostream>
#include <set>
class Package{
public:

    Package();

    Package(ElementID id);

    Package(Package&& p) noexcept;

    Package(const Package& p) noexcept= delete;

    Package& operator=(Package&& p) noexcept ;

    Package& operator=(const Package& p) noexcept = delete;

    ElementID get_id() const {return id_;}

    ~Package();

private:
    inline static std::set<ElementID> assigned_IDs;
    inline static std::set<ElementID> freed_IDs;
    ElementID id_;
};


#endif //SIECI_PACKAGE_HPP
