// Paweł Kolendo, 302860
// Michał Kasperek, 302857
// Michał Gorczyca, 302846

#ifndef SIECI_TYPES_HPP
#define SIECI_TYPES_HPP

#include <functional>
using ElementID=int;
using ProbabilityGenerator=std::function<double()>;
using TimeOffset=int;
using Time=int;
#endif //SIECI_TYPES_HPP