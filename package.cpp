// Paweł Kolendo, 302860
// Michał Kasperek, 302857
// Michał Gorczyca, 302846

#include "package.hpp"
Package::Package(){
    if(!freed_IDs.empty()){
        if(assigned_IDs.empty()){
            id_=1;
            assigned_IDs.insert(id_);
            if(freed_IDs.find(id_)!=freed_IDs.end())freed_IDs.erase(id_);
        } else{
            auto fit=std::min_element(freed_IDs.begin(),freed_IDs.end());
            auto ait=std::max_element(assigned_IDs.begin(),assigned_IDs.end());
            if(*ait+1<*fit){id_=*ait+1;}
            else id_=*fit;
            if(assigned_IDs.find(id_)==assigned_IDs.end())assigned_IDs.insert(id_);
            if(freed_IDs.find(id_)!=freed_IDs.end())freed_IDs.erase(id_);}
    }
    else{
        ElementID count;
        if(assigned_IDs.empty()) count=0;
        else count = *std::max_element(assigned_IDs.begin(),assigned_IDs.end());
        assigned_IDs.insert(count+1);
        id_=count+1;
    }

}

Package::Package(ElementID id){
    id_ = id;
    if(assigned_IDs.find(id_)==assigned_IDs.end())assigned_IDs.insert(id_);
}

Package::Package(Package&& p) noexcept{
    id_=p.id_;
    p.id_=-1;
}

Package& Package::operator= (Package&& p) noexcept {
    if(assigned_IDs.find(id_)!=assigned_IDs.end())assigned_IDs.erase(id_);
    if(freed_IDs.find(id_)==freed_IDs.end())freed_IDs.insert(id_);
    this->id_=p.id_;
    p.id_=-1;
    return (*this);
}
Package::~Package(){
    if(id_==-1)return;
    assigned_IDs.erase(id_);
    if(freed_IDs.find(id_)==freed_IDs.end())freed_IDs.insert(id_);
}
